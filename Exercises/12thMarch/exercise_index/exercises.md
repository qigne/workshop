Lab  for Second Day
=====================

Creating a Document
===================

1.  Handling preamble of the Document, abstract, Comments, and
    displaying the title of your document.

Lists
=====

1.  Creating Unordered list.

2.  Creating Ordered lists.

3.  Creating Nested Lists.

Document Structure
==================

1.  Creating parts, Chapters, sections, subsections.

2.  Creating Numbered and unnumbered parts, Chapters, sections.

3.  Working with Glossaries, Nomenclature and index

4.  Creating Hyperlinks in the document and Linking web addresses.

5.  Inserting Footnotes in Documents

6.  Managing Table of contents of document.

Figures
=======

1.  Inserting figures in the document.

2.  Creating captions, Labels and cross-references.

3.  Scaling Images and rotating images.

Tables
======

1.  Creating tables in the document with variable width.

2.  Creating tables in the document with fixed width.

3.  Creating captions, Labels and cross-references.

4.  Creating multi-row and multi-column tables.

Page Formatting
===============

1.  Creating Headers and Footers of your document

2.  Creating and changing page numbering.

3.  Managing paragraph spacing

4.  Creating multiple columns style

5.  Displaying Code Content using verbatim environment

Mathematics
===========

1.  Inserting Math equation in the Document.

2.  Create equation numbering and cross referencing it.

3.  Create equations using fractions


Lab for Last Day
=======
JabRef
======

1.  Understanding JabRef

2.  Building Database by Manual Entry

3.  Import Bibliographic Database

4.  Adding database using search through various databases

5.  Attaching pdf files to entries

6.  Creating groups

7.  Creating abbreviations

8.  Generating keys

9.  Removing Duplicate entries

10. Searching Database

11. Retrieving References in a Database

Working with BibTeX and Natbib
==============================

-   Exporting database in LaTeX.

-   Working with BibTeX.

-   Natbib and its numbers and author year styles.

Reference Management of Thesis/Research Papers
==============================================

1.  Working with its references

2.  Working with papers and changing styles.


