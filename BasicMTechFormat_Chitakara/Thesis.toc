\gdef \the@ipfilectr {@-1}
\contentsline {section}{\textit {CANDIDATE'S DECLARATION}}{i}{chapter*.3}
\contentsline {section}{\textit {ABSTRACT}}{ii}{chapter*.4}
\contentsline {section}{\textit {ACKNOWLEDGEMENTS}}{iii}{chapter*.5}
\contentsline {section}{\textit {LIST OF FIGURES}}{v}{chapter*.6}
\contentsline {section}{\textit {LIST OF TABLES}}{vi}{chapter*.7}
\contentsline {section}{\textit {ABBREVIATIONS}}{vi}{chapter*.9}
\contentsline {section}{\textit {NOMENCLATURE}}{vii}{chapter*.10}
\contentsline {section}{\textit {TABLE OF CONTENTS}}{vii}{chapter*.10}
\gdef \the@ipfilectr {}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.12}
\contentsline {section}{\numberline {1.1}Use of acronyms and Glossaries}{1}{section.13}
\contentsline {section}{\numberline {1.2}Introduction}{1}{section.15}
\contentsline {section}{\numberline {1.3}Hot Spots}{2}{section.16}
\contentsline {section}{\numberline {1.4}Crime Hot Spots}{2}{section.17}
\contentsline {section}{\numberline {1.5}GIS and Crime mapping}{3}{section.18}
\contentsline {chapter}{References}{4}{section.18}
\gdef \the@ipfilectr {@-2}
\contentsline {chapter}{Appendix A }{5}{appendix*.19}
\vspace {2em}
\gdef \the@ipfilectr {}
